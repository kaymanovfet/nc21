f@f-Lenovo-G500:~/nc21$ git status
На ветке master
Ваша ветка обновлена в соответствии с «origin/master».

Изменения, которые не в индексе для коммита:
  (используйте «git add <файл>…», чтобы добавить файл в индекс)
  (используйте «git checkout -- <файл>…», чтобы отменить изменения
   в рабочем каталоге)

	изменено:      task2.md

Неотслеживаемые файлы:
  (используйте «git add <файл>…», чтобы добавить в то, что будет включено в коммит)

	.readme.md.swp
	.task1.md.swk
	.task1.md.swl
	.task1.md.swm
	.task1.md.swn
	.task1.md.swo
	.task2.md.swn
	.task2.md.swo
	.task2.md.swp

нет изменений добавленных для коммита
(используйте «git add» и/или «git commit -a»)
f@f-Lenovo-G500:~/nc21$ git stash
Рабочий каталог и состояние индекса сохранены WIP on master: ae04be1 вроде сылочка
f@f-Lenovo-G500:~/nc21$ stash list

Command 'stash' not found, did you mean:

  command 'kstash' from deb heimdal-kdc
  command 'sash' from deb sash
  command 'smash' from deb node-smash

Try: sudo apt install <deb name>

f@f-Lenovo-G500:~/nc21$ git stash list
stash@{0}: WIP on master: ae04be1 вроде сылочка
f@f-Lenovo-G500:~/nc21$ git status
На ветке master
Ваша ветка обновлена в соответствии с «origin/master».

Неотслеживаемые файлы:
  (используйте «git add <файл>…», чтобы добавить в то, что будет включено в коммит)

	.readme.md.swp
	.task1.md.swk
	.task1.md.swl
	.task1.md.swm
	.task1.md.swn
	.task1.md.swo
	.task2.md.swn
	.task2.md.swo
	.task2.md.swp

ничего не добавлено в коммит, но есть неотслеживаемые файлы (используйте «git add», чтобы отслеживать их)
f@f-Lenovo-G500:~/nc21$ git chekout develop
git: «chekout» не является командой git. Смотрите «git --help».

Самые похожие команды:
	checkout
f@f-Lenovo-G500:~/nc21$ git checkout develop
Переключено на ветку «develop»
Ваша ветка обновлена в соответствии с «origin/develop».
f@f-Lenovo-G500:~/nc21$ vi task2.md
f@f-Lenovo-G500:~/nc21$ vi task3.md
f@f-Lenovo-G500:~/nc21$ 
_______________________________________________------



f@f-Lenovo-G500:~/nc21$ git commit -m "вернул спрятанное"
На ветке master
Ваша ветка обновлена в соответствии с «origin/master».

Изменения, которые не в индексе для коммита:
	изменено:      task2.md

Неотслеживаемые файлы:
	.readme.md.swp
	.task1.md.swk
	.task1.md.swl
	.task1.md.swm
	.task1.md.swn
	.task1.md.swo
	.task2.md.swn
	.task2.md.swo
	.task2.md.swp

нет изменений добавленных для коммита
f@f-Lenovo-G500:~/nc21$ git add task2.md
f@f-Lenovo-G500:~/nc21$ git commit -m "вернул спрятанное"
[master 826de76] вернул спрятанное
 1 file changed, 1 insertion(+), 1 deletion(-)
f@f-Lenovo-G500:~/nc21$ git status
На ветке master
Ваша ветка опережает «origin/master» на 1 коммит.
  (используйте «git push», чтобы опубликовать ваши локальные коммиты)

Неотслеживаемые файлы:
  (используйте «git add <файл>…», чтобы добавить в то, что будет включено в коммит)

	.readme.md.swp
	.task1.md.swk
	.task1.md.swl
	.task1.md.swm
	.task1.md.swn
	.task1.md.swo
	.task2.md.swn
	.task2.md.swo
	.task2.md.swp

ничего не добавлено в коммит, но есть неотслеживаемые файлы (используйте «git add», чтобы отслеживать их)
f@f-Lenovo-G500:~/nc21$ git merge develop
Merge made by the 'recursive' strategy.
 task3.md | 70 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 1 file changed, 70 insertions(+)
 create mode 100644 task3.md
f@f-Lenovo-G500:~/nc21$ 
:wq

